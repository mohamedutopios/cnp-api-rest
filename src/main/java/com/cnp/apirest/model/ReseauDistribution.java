package com.cnp.apirest.model;

import javax.persistence.*;

@Entity
@Table(name = "reseau_distribution")
public class ReseauDistribution {

    @Id
    @Column(name = "id_reseau", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}