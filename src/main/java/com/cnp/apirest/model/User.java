package com.cnp.apirest.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "user_societe")
public class User {
    @Id
    @Column(name = "id_user", nullable = false)
    private Integer id;

    @Column(name = "identifiant_societe", length = 300)
    private String identifiantSociete;

    @Column(name = "nom_user", length = 100)
    private String nomUser;

    @Column(name = "prenom_user", length = 100)
    private String prenomUser;

    @Column(name = "login_user", length = 100)
    private String loginUser;

    @Column(name = "password_user", length = 100)
    private String passwordUser;

    @Column(name = "date_naissance")
    private LocalDate dateNaissance;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_role")
    private Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getPasswordUser() {
        return passwordUser;
    }

    public void setPasswordUser(String passwordUser) {
        this.passwordUser = passwordUser;
    }

    public String getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(String loginUser) {
        this.loginUser = loginUser;
    }

    public String getPrenomUser() {
        return prenomUser;
    }

    public void setPrenomUser(String prenomUser) {
        this.prenomUser = prenomUser;
    }

    public String getNomUser() {
        return nomUser;
    }

    public void setNomUser(String nomUser) {
        this.nomUser = nomUser;
    }

    public String getIdentifiantSociete() {
        return identifiantSociete;
    }

    public void setIdentifiantSociete(String identifiantSociete) {
        this.identifiantSociete = identifiantSociete;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}