package com.cnp.apirest.service;

import com.cnp.apirest.model.Client;
import com.cnp.apirest.model.Contrat;
import com.cnp.apirest.model.Inscription;
import com.cnp.apirest.model.ReseauDistribution;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface InscriptionService {

    public List<Inscription> getInscriptionByReseau(ReseauDistribution reseau);

    public List<Inscription> getInscriptionByClient(Client client);

    public List<Inscription> getInscriptionByContrat(Contrat contrat);

    public Inscription createInscription(Inscription inscription);

    public Optional<Inscription> getInscriptionById(Integer idInscription);

    public boolean deleteInscription(Integer idInscription);

    public List<Inscription> getInscriptionBetweenDateDebutAndDateFin(Date dateDebut, Date dateFin);

}
