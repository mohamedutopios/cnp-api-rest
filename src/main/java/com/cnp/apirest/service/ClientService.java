package com.cnp.apirest.service;

import com.cnp.apirest.model.Client;

import java.util.List;
import java.util.Optional;

public interface ClientService {

    public List<Client> getListClient();
    public Optional<Client> getClientById(Integer idClient);
    public boolean deleteClient(Integer idClient);
    public Client modifyClient(Client client);
    public Client createClient(Client client);

}
