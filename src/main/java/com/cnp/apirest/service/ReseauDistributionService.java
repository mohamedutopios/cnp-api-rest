package com.cnp.apirest.service;

import com.cnp.apirest.model.ReseauDistribution;

import java.util.List;
import java.util.Optional;

public interface ReseauDistributionService {

    public List<ReseauDistribution> getListReseauDistribution();

    public Optional<ReseauDistribution> getReseauDistributionId(Integer idReseauDistribution);

    public boolean deleteReseauDistribution(Integer idReseauDistribution);

    public ReseauDistribution modifyReseauDistribution(ReseauDistribution reseauDistribution);

    public ReseauDistribution createReseauDistribution(ReseauDistribution reseauDistribution);


}
