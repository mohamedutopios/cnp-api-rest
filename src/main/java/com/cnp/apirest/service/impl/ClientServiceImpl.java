package com.cnp.apirest.service.impl;

import com.cnp.apirest.model.Client;
import com.cnp.apirest.repository.ClientRepository;
import com.cnp.apirest.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientRepository clientRepository;

    @Override
    public List<Client> getListClient() {
        return (List<Client>) clientRepository.findAll();
    }

    @Override
    public Optional<Client> getClientById(Integer idClient) {
        return clientRepository.findById(idClient);
    }

    @Override
    public boolean deleteClient(Integer idClient) {
        boolean clientDelete = true;
        Client client = clientRepository.findById(idClient).get();
        clientRepository.delete(client);
        clientDelete = !clientRepository.findById(idClient).isPresent()? true:false;
        return clientDelete;
    }

    @Override
    public Client modifyClient(Client client) {


        return clientRepository.save(client);
    }

    @Override
    public Client createClient(Client client) {
        return clientRepository.save(client);
    }
}
