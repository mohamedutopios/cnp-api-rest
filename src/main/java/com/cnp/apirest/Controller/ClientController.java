package com.cnp.apirest.Controller;


import com.cnp.apirest.model.Client;
import com.cnp.apirest.model.Inscription;
import com.cnp.apirest.model.Response;
import com.cnp.apirest.service.ClientService;
import com.cnp.apirest.service.InscriptionService;
import com.cnp.apirest.utils.LocalException;
import com.cnp.apirest.utils.RandomString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;


@RestController
@RequestMapping(value="/api/v1")
public class ClientController {

    private static final Logger logger = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    ClientService clientService;

    @Autowired
    InscriptionService inscriptionService;

    @RequestMapping(value="/clients", method = RequestMethod.GET)
    public ResponseEntity<List<Client>> getAllClients(){

        logger.info("----- Methode getAllClient -----");
        List<Client> clients = clientService.getListClient();
        HttpHeaders headers = new HttpHeaders();

        if(!clients.isEmpty()){
            headers.add("Custom-Header", "Client list is not empty");
        }else{
            headers.add("Custom-Header", "Client list is empty");
        }
        return new ResponseEntity<List<Client>>(clients, headers, HttpStatus.OK);
    }


    @RequestMapping(value="/client/{id}", method=RequestMethod.GET)
    public ResponseEntity<Client> getClientById(@PathVariable("id") Integer idClient) throws Exception{

        Optional<Client> client = clientService.getClientById(idClient);

        if(!client.isPresent()){

            throw new Exception("Client not exist");
        }

        logger.info(" ---- Methode getClientById ---");
        logger.info(" ---- L'id du client est : " + idClient);

        return new ResponseEntity<Client>(client.get(), HttpStatus.OK);

    }

    @RequestMapping(value="/client", method = RequestMethod.POST)
    public ResponseEntity<Client> createOrUpdate(@RequestBody Client client){

        logger.info("----- Methode createOrUpdate --- ");

        if(client.getId() != null){
            Client clientModify = clientService.modifyClient(client);
            logger.info(" l'Id du client modifié est : " + clientModify.getId());
            return new ResponseEntity<Client>(clientModify, HttpStatus.OK);
        }else {
            RandomString gen = new RandomString(8, ThreadLocalRandom.current());
            client.setRicNumero(gen.nextString());
            Client client1 = clientService.createClient(client);
            logger.info("L'id du client crée est : " + client1.getId());
            return new ResponseEntity<Client>(client1, HttpStatus.OK);
        }



    }


    @RequestMapping(value = "/delete-client/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Response> deleteClientById(@PathVariable("id") Integer idClient) throws LocalException {

        boolean isDelete = true;
        logger.info("-- L'id du client a supprimé est : " + idClient);
        Optional<Client> client = clientService.getClientById(idClient);

        if (!client.isPresent()) {

            throw new LocalException("Le client n'existe pas");
        }

        Client client1 = clientService.getClientById(idClient).get();
        List<Inscription> inscriptions = inscriptionService.getInscriptionByClient(client1);

        if (!inscriptions.isEmpty()) {
            logger.info("-- Le nombre d'inscription est  : " + inscriptions.size());
            inscriptions.forEach(inscription -> {

                inscriptionService.deleteInscription(inscription.getId());

            });

        }

        List<Inscription> inscription2s = inscriptionService.getInscriptionByClient(client1);

        if (inscription2s.isEmpty()) {
            logger.info("-- Le nombre d'inscription est  : " + inscription2s.size());
            logger.info("-- phase de supression du client");
            isDelete = clientService.deleteClient(idClient);
            logger.info("-- Le client est supprimé : " + isDelete);

        }

        if (!isDelete) {
            return new ResponseEntity<Response>(new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Client not deleted"), HttpStatus.INTERNAL_SERVER_ERROR);
        }


        return new ResponseEntity<Response>(new Response(HttpStatus.OK.value(), "Client has been deleted"), HttpStatus.OK);

    }








}
