package com.cnp.apirest.repository;

import com.cnp.apirest.model.Contrat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ContratRepository extends CrudRepository<Contrat, Integer> {


}
