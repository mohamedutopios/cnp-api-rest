package com.cnp.apirest.repository;

import com.cnp.apirest.model.ReseauDistribution;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ReseauDistributionRepository extends CrudRepository<ReseauDistribution, Integer> {

}
